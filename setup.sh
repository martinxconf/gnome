#!/bin/bash
# setup.sh
#
# gnome
#
# Installs gnome-tweaks:
# 	allows configuration of fonts, etc
# 	through a GUI

set -o nounset
set -e


function status() {
        echo "=== gnome setup ===" "$@"
}

function tryTweakGnomeSettings() {
        which gsettings >/dev/null && (( $? == 0 )) || {
                status "ERROR: skipping Gnome config, gsettings not found"
                        exit 1
                }

        # Caps is a new Esc
        # "['caps:escape']"
        # Caps is a new Esc, Shift+Caps is original Caps
        # "['caps:escape_shifted_capslock']"
        # Swap Caps/Esc
        # "['caps:swapescape']"
        #
        # Set Compose-key to menu-key (next to altGr)
        # 'compose:menu'
        #
        # Swap Caps and Ctrl_L
        # 'ctrl:swapcaps'
        #
        # Find new options:
        # $ grep -E "(ctrl|caps):" /usr/share/X11/xkb/rules/base.lst
        # grep -i -E "(alt|win|meta|super)" /usr/share/X11/xkb/rules/base.lst
        #
        # This works to replace RightWin to Alt
        # 'altwin:alt_super_win' Alt is mapped to Right Win, Super to Menu
        #
        # Can't mix with 'compose:menu' option
        # 'ctrl:menu_rctrl'
        #
        # FULL CONFIGS
        # "['caps:escape', 'compose:paus', 'altwin:alt_super_win', 'ctrl:menu_rctrl']"
        # Description:
        #       - caps is a new Escape
        #       - Pause is the Compose key
        #       - Windows_R key is a new Alt_R key
        #       - Menu key is a new Ctrl_R key
        #

        status "Tweaking gsettings keyboard options"
        gsettings set \
                org.gnome.desktop.input-sources xkb-options \
                "['caps:escape', 'compose:paus', 'altwin:alt_super_win', 'ctrl:menu_rctrl']"

        if (( $? != 0 )); then
                status "ERROR: couldn't set gsettings xkb-options"
                return 1
        fi
}

tryTweakGnomeSettings

status "OK"
